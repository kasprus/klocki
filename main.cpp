#include <iostream>
#include <algorithm>
#include <map>

using namespace std;

class Elementy{
	public:
		long long int getMedium(){
			return medium;
			if(!numberOfElements)return 0;
			while(secondHalf.begin()->second==0)secondHalf.erase(secondHalf.begin());
			return secondHalf.begin()->first;
		}
		void insert(long long int elem){
			if(!numberOfElements){
				numberOfChanges=0;
				++secondHalf[elem];
				++numberOfHigher;
			}
			else if(elem<getMedium()){
				numberOfChanges+=getMedium()-elem;
				++firstHalf[elem];
				++numberOfLower;
			}
			else{
				numberOfChanges+=elem-getMedium();
				++secondHalf[elem];
				++numberOfHigher;
			}
			++numberOfElements;
			//cout<<";;; "<<numberOfChanges<<" ";
			repair();
		}
		void remove(long long int elem){
			--numberOfElements;
			if(firstHalf[elem]!=0){
				--firstHalf[elem];
				--numberOfLower;
				numberOfChanges-=getMedium()-elem;
			}
			else{
				--secondHalf[elem];
				--numberOfHigher;
				numberOfChanges-=elem-getMedium();
			}
			repair();
		}

		long long int getNumberOfChanges(){
			return numberOfChanges;
		}
	private:
		long long int numberOfChanges;
		void repair(){
			while(numberOfLower>numberOfElements/2){
				map<long long int, long long int>::reverse_iterator it = firstHalf.rbegin();
				while(it->second==0){
					firstHalf.erase(it->first);
					it=firstHalf.rbegin();
				}
				while(secondHalf.begin()->second==0)secondHalf.erase(secondHalf.begin());
				long long int tmpmed=medium;
				--firstHalf[it->first];
				++secondHalf[it->first];
				--numberOfLower;
				++numberOfHigher;
				while(secondHalf.begin()->second==0)secondHalf.erase(secondHalf.begin());
				numberOfChanges-=(numberOfLower+1)*(tmpmed-secondHalf.begin()->first);
				numberOfChanges+=(numberOfHigher-1)*(tmpmed-secondHalf.begin()->first);
			//	cout<<"bzzz "<<tmpmed<<" : "<<secondHalf.begin()->first;
			}
			//cout<<numberOfChanges<<" ";
			while(numberOfLower<numberOfElements/2){
				//cout<<"*"<<" ";
				map<long long int, long long int>::iterator it = secondHalf.begin();
				while(it->second==0){
					secondHalf.erase(it);
					it=secondHalf.begin();
				}
				//while(secondHalf.begin()->second==0)secondHalf.erase(secondHalf.begin());
				long long int tmpmed=medium;
				--secondHalf[it->first];
				++firstHalf[it->first];
				++numberOfLower;
				--numberOfHigher;
				while(secondHalf.begin()->second==0){
					secondHalf.erase(secondHalf.begin());
				}
				//cout<<it->second-tmpmed<<"] ";
				numberOfChanges+=(numberOfLower)*(secondHalf.begin()->first-tmpmed);
				numberOfChanges-=(numberOfHigher)*(secondHalf.begin()->first-tmpmed);
			}
			while(secondHalf.begin()->second==0)secondHalf.erase(secondHalf.begin());
			medium=secondHalf.begin()->first;
			//cout<<"tutaj "<<numberOfChanges<<endl;
		}
		map<long long int, long long int>firstHalf;
		long long int numberOfLower;
		map<long long int, long long int>secondHalf;
		long long int numberOfHigher;
		long long int numberOfElements;
		long long int medium;
};

Elementy towers;

long long int arrayOfTowers[100000];
long long int arrayOfMediums[100000];
long long int numberOfChanges[100000];

void countChanges(long long int first, long long int last){
	for(long long int i=0; i<=first; ++i){

	}
}

int main(){
	ios_base::sync_with_stdio(0);
	long long int a, b;
	cin>>a>>b;

	for(long long int i=0; i<a; ++i){
		cin>>arrayOfTowers[i];
	}

	for(long long int i=0; i<b; ++i){
		towers.insert(arrayOfTowers[i]);
	}

	long long int result;
	long long int pos;
	long long int med;

	arrayOfMediums[b-1]=towers.getMedium();
	numberOfChanges[b-1]=towers.getNumberOfChanges();
	pos=b-1;
	result=towers.getNumberOfChanges();
	med=towers.getMedium();


	for(long long int c=b; c<a; ++c){
		towers.insert(arrayOfTowers[c]);
		towers.remove(arrayOfTowers[c-b]);
		if(towers.getNumberOfChanges()<result){
			result=towers.getNumberOfChanges();
			pos=c;
			med=towers.getMedium();
		}
	}

cout<<result<<endl;
for(long long int i=0; i<a; ++i){
	if(i<=pos&&i>pos-b)cout<<med<<endl;
	else{
		cout<<arrayOfTowers[i]<<endl;
	}
}

	return 0;
}
